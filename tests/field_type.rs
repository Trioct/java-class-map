use java_class_map::field_type::*;

macro_rules! parse_assert_ok {
    ($s:expr, $t:expr) => {
        assert_eq!($s.parse::<FieldType>(), Ok($t));
    };
}

macro_rules! parse_assert_err {
    ($s:expr) => {
        assert!($s.parse::<FieldType>().is_err());
    };
}

#[test]
fn test_base_types() {
    macro_rules! base_type_assert {
        ($s:expr, $t:expr) => {
            parse_assert_ok!($s, FieldType::BaseType($t));
        };
    }

    base_type_assert!("B", FieldBaseType::Byte);
    base_type_assert!("C", FieldBaseType::Char);
    base_type_assert!("D", FieldBaseType::Double);
    base_type_assert!("F", FieldBaseType::Float);
    base_type_assert!("I", FieldBaseType::Int);
    base_type_assert!("J", FieldBaseType::Long);
    base_type_assert!("S", FieldBaseType::Short);
    base_type_assert!("Z", FieldBaseType::Boolean);
    parse_assert_err!("W");
}

#[test]
fn test_object_type() {
    parse_assert_ok!("Ltest;", FieldType::ObjectType("test".to_string()));
    parse_assert_err!("Ltest");
}

#[test]
fn test_array_type() {
    parse_assert_ok!("[I", FieldType::BaseArrayType(FieldBaseType::Int, 1));
    parse_assert_ok!(
        "[[Ltest;",
        FieldType::ObjectArrayType("test".to_string(), 2)
    );
    parse_assert_err!("[");
}
