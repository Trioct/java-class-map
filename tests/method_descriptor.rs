use java_class_map::field_type::*;
use java_class_map::method_ref::MethodDescriptor;

macro_rules! parse_assert_ok {
    ($s:expr, $t:expr) => {
        println!("{}", $s);
        assert_eq!($s.parse::<MethodDescriptor>(), Ok($t));
    };
}

macro_rules! parse_assert_err {
    ($s:expr) => {
        println!("{}", $s);
        assert!($s.parse::<MethodDescriptor>().is_err());
    };
}

#[test]
fn test_successes() {
    parse_assert_ok!(
        "()V",
        MethodDescriptor {
            parameters: Vec::new(),
            return_type: None,
        }
    );

    let int_type = "I".parse::<FieldType>().unwrap();
    parse_assert_ok!(
        "(II)I",
        MethodDescriptor {
            parameters: vec![int_type.clone(); 2],
            return_type: Some(int_type.clone()),
        }
    );

    let double_array_test_type = "[[LTest;".parse::<FieldType>().unwrap();
    let double_type = "D".parse::<FieldType>().unwrap();
    let bool_type = "Z".parse::<FieldType>().unwrap();
    let test_type = "LTest;".parse::<FieldType>().unwrap();
    parse_assert_ok!(
        "([[LTest;IDZ)LTest;",
        MethodDescriptor {
            parameters: vec![double_array_test_type, int_type, double_type, bool_type],
            return_type: Some(test_type),
        }
    );
}

#[test]
fn test_failures() {
    parse_assert_err!("()W");
    parse_assert_err!("()");
    parse_assert_err!("V");
    parse_assert_err!("(LTest)V");
    parse_assert_err!("([)V");
    parse_assert_err!("()[V");
    parse_assert_err!("()IV");
}
