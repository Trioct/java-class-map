use crate::constant_pool_error::ConstantPoolError;
use nom::types::CompleteStr;
use std::str::FromStr;

#[derive(Clone, Debug, PartialEq)]
pub enum FieldType {
    BaseType(FieldBaseType),
    ObjectType(String),

    BaseArrayType(FieldBaseType, usize),
    ObjectArrayType(String, usize),
}

#[derive(Clone, Debug, PartialEq)]
pub enum FieldBaseType {
    Byte,
    Char,
    Double,
    Float,
    Int,
    Long,
    Short,
    Boolean,
}

impl FromStr for FieldType {
    type Err = ConstantPoolError;

    fn from_str(s: &str) -> Result<FieldType, ConstantPoolError> {
        Ok(parse_field_type(CompleteStr(s))?.1)
    }
}

named!(pub(crate) parse_field_type<CompleteStr, FieldType>, alt!(
        do_parse!(
            field_base_type: parse_field_base_type >>
            (FieldType::BaseType(field_base_type))
            ) |
        do_parse!(
            field_object_type: parse_field_object_type >>
            (FieldType::ObjectType(field_object_type))
            ) |
        parse_field_array_type
));

named!(parse_field_base_type<CompleteStr, FieldBaseType>,
       switch!(take!(1),
               CompleteStr("B") => value!(FieldBaseType::Byte) |
               CompleteStr("C") => value!(FieldBaseType::Char) |
               CompleteStr("D") => value!(FieldBaseType::Double) |
               CompleteStr("F") => value!(FieldBaseType::Float) |
               CompleteStr("I") => value!(FieldBaseType::Int) |
               CompleteStr("J") => value!(FieldBaseType::Long) |
               CompleteStr("S") => value!(FieldBaseType::Short) |
               CompleteStr("Z") => value!(FieldBaseType::Boolean)
       )
);

named!(parse_field_object_type<CompleteStr, String>,
       do_parse!(
           tag!("L") >>
           class_name: take_until!(";") >>
           tag!(";") >>

           (class_name.to_string())
       )
);

named!(parse_field_array_type<CompleteStr, FieldType>,
       do_parse!(
           depth: many1_count!(tag!("[")) >>
           field_type: alt_complete!(do_parse!(
                   field_base_type: parse_field_base_type >>
                   (FieldType::BaseArrayType(field_base_type, depth))
                   ) |
               do_parse!(
                   field_object_type: parse_field_object_type >>
                   (FieldType::ObjectArrayType(field_object_type, depth))
               )) >>

           (field_type)
       )
);
