use crate::attribute_map::AttributeMap;
use crate::class_descriptor::ClassDescriptor;
use crate::constant_pool_error::ConstantPoolError;
use crate::constant_type::ConstantType;
use crate::field_info::FieldInfo;
use crate::field_ref::FieldRef;
use crate::method_info::MethodInfo;
use crate::method_ref::MethodRef;
use classfile_parser::class_parser;
use classfile_parser::constant_info::*;
use classfile_parser::ClassFile;
use std::collections::HashMap;

#[derive(Clone, Debug, Default)]
pub struct ClassMap {
    pub minor_version: u16,
    pub major_version: u16,

    pub this_class: u16,
    pub super_class: u16,

    pub constants: HashMap<u16, ConstantType>,
    pub classes: HashMap<u16, ClassDescriptor>,
    pub method_refs: HashMap<u16, MethodRef>,
    pub field_refs: HashMap<u16, FieldRef>,

    pub methods: Vec<MethodInfo>,
    pub fields: Vec<FieldInfo>,

    pub attributes: AttributeMap,
}

impl ClassMap {
    pub fn from(bytes: &[u8]) -> Result<ClassMap, ConstantPoolError> {
        let class_file = class_parser(bytes)?.1;

        let mut class_map = ClassMap::default();

        class_map.minor_version = class_file.minor_version;
        class_map.major_version = class_file.major_version;

        class_map.this_class = class_file.this_class;
        class_map.super_class = class_file.super_class;

        class_map.get_constants(&class_file.const_pool)?;
        class_map.get_methods(&class_file)?;
        class_map.get_fields(&class_file)?;

        class_map.attributes = AttributeMap::from(&class_file.const_pool, &class_file.attributes)?;

        Ok(class_map)
    }

    fn get_constants(&mut self, const_pool: &[ConstantInfo]) -> Result<(), ConstantPoolError> {
        for (index, info) in const_pool.iter().enumerate() {
            let insert_index = (index + 1) as u16;
            match info {
                ConstantInfo::Class(inner) => {
                    self.classes
                        .insert(insert_index, ClassDescriptor::from(const_pool, inner)?);
                }
                ConstantInfo::String(inner) => {
                    self.constants.insert(
                        insert_index,
                        ConstantType::from_string_constant(const_pool, inner)?,
                    );
                }
                ConstantInfo::Integer(inner) => {
                    self.constants
                        .insert(insert_index, ConstantType::from(inner));
                }
                ConstantInfo::Float(inner) => {
                    self.constants
                        .insert(insert_index, ConstantType::from(inner));
                }
                ConstantInfo::Long(inner) => {
                    self.constants
                        .insert(insert_index, ConstantType::from(inner));
                }
                ConstantInfo::Double(inner) => {
                    self.constants
                        .insert(insert_index, ConstantType::from(inner));
                }
                ConstantInfo::MethodRef(inner) => {
                    self.method_refs
                        .insert(insert_index, MethodRef::from(const_pool, inner)?);
                }
                ConstantInfo::InterfaceMethodRef(inner) => {
                    self.method_refs
                        .insert(insert_index, MethodRef::from_interface(const_pool, inner)?);
                }
                ConstantInfo::FieldRef(inner) => {
                    self.field_refs
                        .insert(insert_index, FieldRef::from(const_pool, inner)?);
                }
                _ => {}
            }
        }

        Ok(())
    }

    fn get_methods(&mut self, class_file: &ClassFile) -> Result<(), ConstantPoolError> {
        for method in class_file.methods.iter() {
            let method_info = MethodInfo::from(&class_file.const_pool, method)?;
            self.methods.push(method_info);
        }

        Ok(())
    }

    fn get_fields(&mut self, class_file: &ClassFile) -> Result<(), ConstantPoolError> {
        for field in class_file.fields.iter() {
            let field_info = FieldInfo::from(&class_file.const_pool, field)?;
            self.fields.push(field_info);
        }

        Ok(())
    }
}
