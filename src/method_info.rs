use crate::attribute_map::AttributeMap;
use crate::constant_pool_error::ConstantPoolError;
use crate::method_ref::*;
use crate::util::*;
use classfile_parser::constant_info::ConstantInfo;
use classfile_parser::method_info::MethodAccessFlags;

#[derive(Clone, Debug)]
pub struct MethodInfo {
    pub access_flags: MethodAccessFlags,
    pub name: String,
    pub descriptor: MethodDescriptor,
    pub attributes: AttributeMap,
}

impl MethodInfo {
    pub fn from(
        const_pool: &[ConstantInfo],
        method_info: &classfile_parser::method_info::MethodInfo,
    ) -> Result<MethodInfo, ConstantPoolError> {
        let name = expect_utf8(const_pool, method_info.name_index)?;
        let descriptor = expect_utf8(const_pool, method_info.descriptor_index)?
            .utf8_string
            .parse::<MethodDescriptor>()?;
        let attributes = AttributeMap::from(const_pool, &method_info.attributes)?;

        Ok(MethodInfo {
            access_flags: method_info.access_flags,
            name: name.utf8_string.clone(),
            descriptor,
            attributes,
        })
    }
}
