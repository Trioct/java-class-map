use crate::constant_pool_error::ConstantPoolError;
use classfile_parser::constant_info::*;
use std::convert::From;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct ConstantPoolIndex(usize);

impl From<u16> for ConstantPoolIndex {
    fn from(index: u16) -> ConstantPoolIndex {
        ConstantPoolIndex((index - 1) as usize)
    }
}

impl From<ConstantPoolIndex> for usize {
    fn from(index: ConstantPoolIndex) -> usize {
        index.0
    }
}

macro_rules! expect_type {
    ($func_name:ident, $info_type:ident, $ok_type:ty, $err_type:ident) => {
        pub fn $func_name<I: Into<ConstantPoolIndex>>(
            const_pool: &[ConstantInfo],
            index: I,
        ) -> Result<&$ok_type, ConstantPoolError> {
            match &const_pool[index.into().0] {
                ConstantInfo::$info_type(inner) => Ok(&inner),
                _ => Err(ConstantPoolError::$err_type),
            }
        }
    };
}

expect_type!(expect_class, Class, ClassConstant, ExpectedClass);
expect_type!(expect_utf8, Utf8, Utf8Constant, ExpectedUtf8);
expect_type!(
    expect_name_and_type,
    NameAndType,
    NameAndTypeConstant,
    ExpectedNameAndType
);
