use crate::constant_pool_error::ConstantPoolError;
use crate::util::*;
use classfile_parser::constant_info::*;
use std::convert::From;

#[derive(Clone, Debug)]
pub enum ConstantType {
    ConstantString(String),
    ConstantInt(i32),
    ConstantFloat(f32),
    ConstantLong(i64),
    ConstantDouble(f64),
}

impl ConstantType {
    pub fn from_string_constant(
        const_pool: &[ConstantInfo],
        string_constant: &StringConstant,
    ) -> Result<ConstantType, ConstantPoolError> {
        Ok(ConstantType::ConstantString(
            expect_utf8(const_pool, string_constant.string_index)?
                .utf8_string
                .clone(),
        ))
    }
}

macro_rules! impl_from {
    ($const_type:ty, $my_const_type:ident) => {
        impl From<&$const_type> for ConstantType {
            fn from(const_type: &$const_type) -> ConstantType {
                ConstantType::$my_const_type(const_type.value)
            }
        }
    };
}

impl_from!(IntegerConstant, ConstantInt);
impl_from!(FloatConstant, ConstantFloat);
impl_from!(LongConstant, ConstantLong);
impl_from!(DoubleConstant, ConstantDouble);
