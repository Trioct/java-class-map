use std::convert::From;

#[derive(Clone, Debug, PartialEq)]
pub enum ConstantPoolError {
    ExpectedClass,
    ExpectedUtf8,
    ExpectedInt,
    ExpectedFloat,
    ExpectedLong,
    ExpectedDouble,
    ExpectedNameAndType,
    NomError(String),
}

impl<T: std::fmt::Debug> From<nom::Err<T>> for ConstantPoolError {
    fn from(err: nom::Err<T>) -> ConstantPoolError {
        ConstantPoolError::NomError(format!("{}", err))
    }
}
