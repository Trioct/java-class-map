use crate::code_info::CodeInfo;
use crate::constant_pool_error::ConstantPoolError;
use crate::util::*;
use classfile_parser::attribute_info::*;
use classfile_parser::constant_info::*;

#[derive(Clone, Debug, Default)]
pub struct AttributeMap {
    pub code_info: Option<Box<CodeInfo>>,
    pub stack_map_table: Option<StackMapTableAttribute>,
}

impl AttributeMap {
    pub fn from(
        const_pool: &[ConstantInfo],
        attributes: &[AttributeInfo],
    ) -> Result<AttributeMap, ConstantPoolError> {
        let mut attribute_map = AttributeMap::default();

        for attribute in attributes {
            let name = expect_utf8(const_pool, attribute.attribute_name_index)?
                .utf8_string
                .as_str();
            match name {
                "Code" => {
                    attribute_map.code_info =
                        Some(Box::new(CodeInfo::from(const_pool, attribute)?));
                }
                "StackMapTable" => {
                    attribute_map.stack_map_table =
                        Some(stack_map_table_attribute_parser(&attribute.info)?.1);
                }
                _ => {} /*unimplemented!("{:?}", attribute),*/
            }
        }

        Ok(attribute_map)
    }
}
