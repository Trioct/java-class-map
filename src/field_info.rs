use crate::attribute_map::AttributeMap;
use crate::constant_pool_error::ConstantPoolError;
use crate::field_type::FieldType;
use crate::util::*;
use classfile_parser::constant_info::ConstantInfo;
use classfile_parser::field_info::FieldAccessFlags;

#[derive(Clone, Debug)]
pub struct FieldInfo {
    pub access_flags: FieldAccessFlags,
    pub name: String,
    pub field_type: FieldType,
    pub attributes: AttributeMap,
}

impl FieldInfo {
    pub fn from(
        const_pool: &[ConstantInfo],
        field_info: &classfile_parser::field_info::FieldInfo,
    ) -> Result<FieldInfo, ConstantPoolError> {
        let name = expect_utf8(const_pool, field_info.name_index)?;
        let field_type = expect_utf8(const_pool, field_info.descriptor_index)?
            .utf8_string
            .parse::<FieldType>()?;
        let attributes = AttributeMap::from(const_pool, &field_info.attributes)?;
        Ok(FieldInfo {
            access_flags: field_info.access_flags,
            name: name.utf8_string.clone(),
            field_type,
            attributes,
        })
    }
}
