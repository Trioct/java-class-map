use crate::constant_pool_error::ConstantPoolError;
use crate::field_type::*;
use crate::util::*;
use classfile_parser::constant_info::*;
use nom::types::CompleteStr;
use std::str::FromStr;

#[derive(Clone, Debug)]
pub struct MethodRef {
    pub class_name: String,
    pub name: String,
    pub descriptor: MethodDescriptor,
}

impl MethodRef {
    pub fn from(
        const_pool: &[ConstantInfo],
        method_ref_info: &MethodRefConstant,
    ) -> Result<MethodRef, ConstantPoolError> {
        MethodRef::from_indices(
            const_pool,
            method_ref_info.class_index,
            method_ref_info.name_and_type_index,
        )
    }

    pub fn from_interface(
        const_pool: &[ConstantInfo],
        method_ref_info: &InterfaceMethodRefConstant,
    ) -> Result<MethodRef, ConstantPoolError> {
        MethodRef::from_indices(
            const_pool,
            method_ref_info.class_index,
            method_ref_info.name_and_type_index,
        )
    }

    fn from_indices(
        const_pool: &[ConstantInfo],
        class_index: u16,
        name_and_type_index: u16,
    ) -> Result<MethodRef, ConstantPoolError> {
        let class_constant = expect_class(const_pool, class_index)?;
        let class_name = expect_utf8(const_pool, class_constant.name_index)?;

        let name_and_type = expect_name_and_type(const_pool, name_and_type_index)?;
        let name = expect_utf8(const_pool, name_and_type.name_index)?;
        let descriptor_string = expect_utf8(const_pool, name_and_type.descriptor_index)?;

        let descriptor = descriptor_string.utf8_string.parse::<MethodDescriptor>()?;

        Ok(MethodRef {
            class_name: class_name.utf8_string.clone(),
            name: name.utf8_string.clone(),
            descriptor,
        })
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct MethodDescriptor {
    pub parameters: Vec<FieldType>,
    pub return_type: Option<FieldType>,
}

impl FromStr for MethodDescriptor {
    type Err = ConstantPoolError;

    fn from_str(s: &str) -> Result<MethodDescriptor, ConstantPoolError> {
        let parse_result = exact!(
            CompleteStr(s),
            do_parse!(
                parameters: parse_method_parameters
                    >> return_type: parse_method_return_type
                    >> (MethodDescriptor {
                        parameters,
                        return_type,
                    })
            )
        );
        Ok(parse_result?.1)
    }
}

named!(parse_method_parameters<CompleteStr, Vec<FieldType> >,
       do_parse!(
           tag!("(") >>
           inner: many0!(parse_field_type) >>
           tag!(")") >>
           (inner)
       )
);

named!(parse_method_return_type<CompleteStr, Option<FieldType> >,
       alt!(map!(parse_field_type, |f| Some(f)) |
            do_parse!(
                tag!("V") >>
                (None)
            )
       )
);
