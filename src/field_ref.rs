use crate::constant_pool_error::ConstantPoolError;
use crate::field_type::*;
use crate::util::*;
use classfile_parser::constant_info::*;

#[derive(Clone, Debug)]
pub struct FieldRef {
    pub class_name: String,
    pub name: String,
    pub field_type: FieldType,
}

impl FieldRef {
    pub fn from(
        const_pool: &[ConstantInfo],
        field_ref_info: &FieldRefConstant,
    ) -> Result<FieldRef, ConstantPoolError> {
        let class_constant = expect_class(const_pool, field_ref_info.class_index)?;
        let class_name = expect_utf8(const_pool, class_constant.name_index)?;

        let name_and_type = expect_name_and_type(const_pool, field_ref_info.name_and_type_index)?;
        let name = expect_utf8(const_pool, name_and_type.name_index)?;
        let descriptor = expect_utf8(const_pool, name_and_type.descriptor_index)?;

        let field_type = descriptor.utf8_string.parse::<FieldType>()?;

        Ok(FieldRef {
            class_name: class_name.utf8_string.clone(),
            name: name.utf8_string.clone(),
            field_type,
        })
    }
}
