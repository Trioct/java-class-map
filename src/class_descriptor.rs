use crate::constant_pool_error::ConstantPoolError;
use crate::util::*;
use classfile_parser::constant_info::*;

#[derive(Clone, Debug)]
pub struct ClassDescriptor {
    pub name: String,
    pub array_depth: usize,
}

impl ClassDescriptor {
    pub fn from(
        const_pool: &[ConstantInfo],
        class_constant: &ClassConstant,
    ) -> Result<ClassDescriptor, ConstantPoolError> {
        let descriptor = expect_utf8(const_pool, class_constant.name_index)?
            .utf8_string
            .as_str();

        let array_depth = descriptor.chars().take_while(|&c| c == '[').count();
        let name = if array_depth == 0 {
            descriptor
                .chars()
                .skip_while(|&c| c == '[')
                .take_while(|&c| c != ';')
                .collect()
        } else {
            descriptor
                .chars()
                .skip_while(|&c| c == '[')
                .skip(1) // skips L in [LClassName;
                .take_while(|&c| c != ';')
                .collect()
        };

        Ok(ClassDescriptor { name, array_depth })
    }
}
