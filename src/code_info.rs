use crate::attribute_map::AttributeMap;
use crate::constant_pool_error::ConstantPoolError;
use classfile_parser::attribute_info::{code_attribute_parser, AttributeInfo, ExceptionEntry};
use classfile_parser::code_attribute::{code_parser, Instruction};
use classfile_parser::constant_info::ConstantInfo;

#[derive(Clone, Debug)]
pub struct CodeInfo {
    pub max_stack: u16,
    pub max_locals: u16,
    pub code: Vec<(usize, Instruction)>,
    pub exception_table: Vec<ExceptionEntry>,
    pub attributes: AttributeMap,
}

impl CodeInfo {
    pub fn from(
        const_pool: &[ConstantInfo],
        attribute: &AttributeInfo,
    ) -> Result<CodeInfo, ConstantPoolError> {
        let code_attribute = code_attribute_parser(&attribute.info)?.1;
        Ok(CodeInfo {
            max_stack: code_attribute.max_stack,
            max_locals: code_attribute.max_locals,
            code: code_parser(&code_attribute.code)?.1,
            exception_table: code_attribute.exception_table,
            attributes: AttributeMap::from(const_pool, &code_attribute.attributes)?,
        })
    }
}
